# Assignment 3.12 

### How it works
This is a Spring Boot App where we add some structure and redefine
a base project which we made a few days earlier.

The app simulates the usage of controller methods with the use of our old
friend Postman application.

We have a base URL which is http://localhost:8080/api/v1/guitars

####GET Request the full list of the guitars:
http://localhost:8080/api/v1/guitars
Shows the current guitarlist.

####GET Request a specific guitar by it's id:
http://localhost:8080/api/v1/guitars/(id)
Gets the specific guitar from the id number.

####GET Request a list of guitarbrands:
http://localhost:8080/api/v1/guitars/brands
Get you a list of brands and how many of each individual guitars of the specific brand you have.

####POST Create a new guitar:
http://localhost:8080/api/v1/guitars
Creates a new guitar but you must follow the template otherwise you will get an exception.

####PUT Replace a guitar:
http://localhost:8080/api/v1/guitars/(id)
Targets the specified id that u put in the url and replaces it if you followed the template otherwise an exception will be made.

####PATCH Change properties in a guitar:
http://localhost:8080/api/v1/guitars/(id)
Targets the specified id that u put in the url and replaces it if you followed the template and use a (id) that is in use,
otherwise an exception will be made.

####DELETE Delete guitar:
http://localhost:8080/api/v1/guitars/(id)
Deletes the targeted guitar with the use of your (id) input in the url if it exists.


###About
This is an assignment where we had to change the structure from a (List) into a (Map).
Then we had to use the inbuilt DI container to inject GuitarRepository into the GuitarController.
Add ResponseEntitys with appropiate HTTP status codes and try them in GuitarController.
Lastly but absolutely not the easiest here was to create and DTO for my "GET/brands" and move the method for that into "GuitarDtoMapper".

//Jerry Lindström