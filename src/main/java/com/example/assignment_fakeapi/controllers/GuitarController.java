package com.example.assignment_fakeapi.controllers;

import com.example.assignment_fakeapi.dataaccess.InterfaceGuitarRepository;
import com.example.assignment_fakeapi.models.domain.Guitar;
import com.example.assignment_fakeapi.models.dto.GuitarBrandsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


@RestController
@RequestMapping(value = "/api/v1/guitars")
public class GuitarController {
    @Autowired
    private InterfaceGuitarRepository guitarRepository;

    @GetMapping("")
    public ResponseEntity<HashMap<Integer, Guitar>> getAllGuitars() {
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExist(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

    @GetMapping("/brands")
    public ResponseEntity<GuitarBrandsDto> brandList() {
        return new ResponseEntity<>(guitarRepository.getGuitarBrandCounter(), HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<Guitar> createGuitar(@RequestBody Guitar guitar) {
        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(guitarRepository.addGuitar(guitar),HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Guitar> replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if(!guitarRepository.isValidGuitar(guitar) || id != guitar.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!guitarRepository.guitarExist(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        guitarRepository.replaceGuitar(guitar);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Guitar> modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if(!guitarRepository.isValidGuitar(guitar) || id != guitar.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!guitarRepository.guitarExist(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        guitarRepository.modifyGuitar(guitar);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExist(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        guitarRepository.deleteGuitar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
