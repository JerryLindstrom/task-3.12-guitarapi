package com.example.assignment_fakeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentFakeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentFakeApiApplication.class, args);
    }

}
