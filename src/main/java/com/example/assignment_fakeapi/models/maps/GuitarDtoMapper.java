package com.example.assignment_fakeapi.models.maps;

import com.example.assignment_fakeapi.models.domain.Guitar;
import com.example.assignment_fakeapi.models.dto.GuitarBrandsDto;

import java.util.HashMap;

public class GuitarDtoMapper {

    private static int guitarBrandCount(HashMap<Integer, Guitar> guitars, String brand) {
        return (int) guitars
                .values()
                .stream()
                .filter(g -> g.getBrand().equals(brand))
                .count();
    }

    public static GuitarBrandsDto mapGuitarBrandsDto(HashMap<Integer, Guitar> guitars) {
        HashMap<String, Integer> brandsMapper = new HashMap<>();
        for (Guitar guitar:guitars.values()) {
            String brand = guitar.getBrand();
            brandsMapper.put(brand, guitarBrandCount(guitars, brand));
        }
        return new GuitarBrandsDto(brandsMapper);
    }
}
