package com.example.assignment_fakeapi.models.dto;

import java.util.HashMap;

public class GuitarBrandsDto {
    private HashMap<String, Integer> guitarBrandCounter;

    public GuitarBrandsDto(HashMap<String, Integer> guitarBrandCounter) {
        this.guitarBrandCounter = guitarBrandCounter;
    }

    public HashMap<String, Integer> getGuitarBrandCounter() {
        return guitarBrandCounter;
    }

    public void setGuitarBrandCounter(HashMap<String, Integer> guitarBrandCounter) {
        this.guitarBrandCounter = guitarBrandCounter;
    }
}

