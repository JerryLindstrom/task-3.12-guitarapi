package com.example.assignment_fakeapi.dataaccess;

import com.example.assignment_fakeapi.models.domain.Guitar;
import com.example.assignment_fakeapi.models.dto.GuitarBrandsDto;
import com.example.assignment_fakeapi.models.maps.GuitarDtoMapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class GuitarRepository implements InterfaceGuitarRepository{
    private HashMap<Integer, Guitar> guitars = initialGuitarList();

    private HashMap<Integer, Guitar> initialGuitarList() {
        var guitars = new HashMap<Integer, Guitar>();

        guitars.put(1, new Guitar(1, "Gibson", "Les Paul"));
        guitars.put(2, new Guitar(2, "Fender", "Telecaster"));
        guitars.put(3, new Guitar(3, "Gibson", "Junior"));
        guitars.put(4, new Guitar(4, "Gibson", "J-200"));

        return guitars;
    }

    @Override
    public HashMap<Integer, Guitar> getAllGuitars() {
        return guitars;
    }

    @Override
    public Guitar getGuitar(int id) {
        return guitars.get(id);
    }

    @Override
    public Guitar addGuitar(Guitar guitar) {
        guitars.put(guitar.getId(), guitar);
        return guitars.get(guitar.getId());
    }

    @Override
    public Guitar replaceGuitar(Guitar guitar) {
        var guitarToDelete = getGuitar(guitar.getId());
        guitars.remove(guitarToDelete);

        guitars.put(guitar.getId(), guitar);
        return getGuitar(guitar.getId());
    }

    @Override
    public Guitar modifyGuitar(Guitar guitar) {
        var guitarToModify = getGuitar(guitar.getId());

        guitarToModify.setModel(guitar.getModel());
        guitarToModify.setBrand(guitar.getBrand());

        return  guitarToModify;
    }

    @Override
    public void deleteGuitar(int id) {
        guitars.remove(id);
    }

    @Override
    public boolean guitarExist(int id) {
        return getGuitar(id) != null;
    }

    @Override
    public boolean isValidGuitar(Guitar guitar) {
        return guitar.getId() > 0 && !guitar.getBrand().equals(null) && !guitar.getModel().equals(null);
    }

    @Override
    public GuitarBrandsDto getGuitarBrandCounter() {
        return GuitarDtoMapper.mapGuitarBrandsDto(guitars);
    }
}
