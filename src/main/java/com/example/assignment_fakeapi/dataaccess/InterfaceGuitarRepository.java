package com.example.assignment_fakeapi.dataaccess;

import com.example.assignment_fakeapi.models.domain.Guitar;
import com.example.assignment_fakeapi.models.dto.GuitarBrandsDto;

import java.util.HashMap;

public interface InterfaceGuitarRepository {
    HashMap<Integer, Guitar> getAllGuitars();
    Guitar getGuitar(int id);
    Guitar addGuitar(Guitar guitar);
    Guitar replaceGuitar(Guitar guitar);
    Guitar modifyGuitar(Guitar guitar);
    void deleteGuitar(int id);
    boolean guitarExist(int id);
    boolean isValidGuitar(Guitar guitar);
    GuitarBrandsDto getGuitarBrandCounter();
}
